import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadastroComponent } from './cadastro/cadastro.component';
import { AdicionarComponent } from './adicionar/adicionar.component';
import { Error404Component } from './error404/error404.component';


const routes: Routes = [
  { path: '', component: CadastroComponent },
  { path: 'home', component: CadastroComponent },
  { path: 'adicionar', component: AdicionarComponent },
  {  path: '**', component: Error404Component }

];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)],
})

export class AppRoutingModule {}
