import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtroBusca'
})
export class FiltroBuscaPipe implements PipeTransform {

  transform(colaboradores, digitado: string ) {
    digitado = digitado.toLowerCase();
    return colaboradores.filter( colaboradores => colaboradores.nome.toLowerCase().includes(digitado));  
}

}
