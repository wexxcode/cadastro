import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CadastroComponent } from './cadastro/cadastro.component';
import { HeaderComponent } from './header/header.component';
import { AdicionarComponent } from './adicionar/adicionar.component';
import { AppRoutingModule } from './/app-routing.module';
import { Error404Component } from './error404/error404.component';
import { FiltroBuscaPipe } from './filtro-busca.pipe';

@NgModule({
  declarations: [
    AppComponent,
    CadastroComponent,
    HeaderComponent,
    AdicionarComponent,
    Error404Component,
    FiltroBuscaPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
