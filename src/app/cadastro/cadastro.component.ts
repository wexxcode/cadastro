import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {
  
  colaboradores = [
    {
      "nome":"Adriano da Silva Andrade",
      "contato": {
        "telefone":"61 - 99274 3753",
        "email":"wexxcode@gmail.com"
      }
    },
    {
      "nome":"Lucas Pereira",
      "contato": {
        "telefone":"61 - 99274 3753",
        "email":"wexxcode@gmail.com"
      }
    },
    {
      "nome":"André Vargas",
      "contato": {
        "telefone":"61 - 99274 3753",
        "email":"wexxcode@gmail.com"
      }
    },
    {
      "nome":"Rafael da Silva",
      "contato": {
        "telefone":"61 - 99274 - 3753",
        "email":"wexxcode@gmail.com"
      }
    },
    {
      "nome":"Rafael da Silva",
      "contato": {
        "telefone":"61 - 99274 - 3753",
        "email":"wexxcode@gmail.com"
      }
    },
    {
      "nome":"Rafael da Silva",
      "contato": {
        "telefone":"61 - 99274 - 3753",
        "email":"wexxcode@gmail.com"
      }
    },
    {
      "nome":"Rafael da Silva",
      "contato": {
        "telefone":"61 - 99274 - 3753",
        "email":"wexxcode@gmail.com"
      }
    },
    ];
  
  constructor() {}

  ngOnInit() {
  }

}
